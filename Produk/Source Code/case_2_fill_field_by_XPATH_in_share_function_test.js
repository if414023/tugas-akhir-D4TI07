Feature('Case 13 Fill Field with Tap Function in Share Function');

Scenario('Test Share', (I) => {
	I.wait(3);

	I.tap("//android.widget.Button[@resource-id='com.android.packageinstaller:id/permission_allow_button']");

	I.wait(1);

	I.tap("//android.widget.Button[@resource-id='com.android.packageinstaller:id/permission_allow_button']");

	I.wait(1);

	I.tap("//android.widget.Button[@resource-id='com.android.packageinstaller:id/permission_allow_button']");

	I.wait(1);

	I.tap("//android.widget.Button[@resource-id='com.android.packageinstaller:id/permission_allow_button']");

	I.wait(1);
	
	I.tap("//android.widget.ImageView[@resource-id='praktikum.develops.a11414001.digitourapplication:id/btnImageShareFacebook']");

	I.wait(2);

	I.fillField("//android.widget.EditText[@resource-id='com.facebook.katana:id/(name removed)']",'Saya mau share ke facebook dengan cara fill field');

	I.wait(1);

	I.tap("//android.widget.Button[@text='POST']");

	I.wait(3);
});