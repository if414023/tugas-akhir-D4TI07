Feature('Case 11 Fill Field in Share Function');

Scenario('Test Share', (I) => {
	I.wait(3);

	I.tap("//android.widget.Button[@resource-id='com.android.packageinstaller:id/permission_allow_button']");

	I.wait(1);

	I.tap("//android.widget.Button[@resource-id='com.android.packageinstaller:id/permission_allow_button']");

	I.wait(1);

	I.tap("//android.widget.Button[@resource-id='com.android.packageinstaller:id/permission_allow_button']");

	I.wait(1);

	I.tap("//android.widget.Button[@resource-id='com.android.packageinstaller:id/permission_allow_button']");

	I.wait(1);
	
	I.tap("//android.widget.ImageView[@resource-id='praktikum.develops.a11414001.digitourapplication:id/btnImageShareFacebook']");

	I.wait(2);

	I.fillField('Write something…','Saya mau share ke facebook dengan cara fill field');

	I.wait(1);

	I.tap("//android.widget.Button[@text='POST']");

	I.wait(3);
});