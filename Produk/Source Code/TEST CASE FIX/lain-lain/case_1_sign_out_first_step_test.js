Feature('Case 8 Sign Out (First Step) With Click Function by get XPath');

Scenario('Test Sign Out', (I) => {
	I.wait(3);

	I.click("//android.widget.Button[@resource-id='com.android.packageinstaller:id/permission_allow_button']");

	I.wait(1);

	I.click("//android.widget.Button[@resource-id='com.android.packageinstaller:id/permission_allow_button']");

	I.wait(1);

	I.click("//android.widget.Button[@resource-id='com.android.packageinstaller:id/permission_allow_button']");

	I.wait(1);

	I.click("//android.widget.Button[@resource-id='com.android.packageinstaller:id/permission_allow_button']");

	I.wait(1);

	I.click("//android.widget.ImageView[@resource-id='praktikum.develops.a11414001.digitourapplication:id/imageViewPlay']");

	I.wait(2);

	I.click("//android.widget.Button[@text='Sign in']");

	I.wait(2);

	I.click("//android.widget.TextView[@text='Johannes Christian Sitorus']");

	I.wait(5);

	I.click("//android.widget.Button[@resource-id='praktikum.develops.a11414001.digitourapplication:id/btnsign_out']");	

	I.wait(1);

	I.click("//android.widget.ImageView[@resource-id='praktikum.develops.a11414001.digitourapplication:id/imageViewPlay']");

	I.wait(3);
});