Feature('Case Access Location Siallagan With Tap Function by get XPath');

Scenario('Test Access Location', (I) => {
	I.wait(3);

	I.tap("//android.widget.Button[@resource-id='com.android.packageinstaller:id/permission_allow_button']");

	I.wait(1);

	I.tap("//android.widget.Button[@resource-id='com.android.packageinstaller:id/permission_allow_button']");

	I.wait(1);

	I.tap("//android.widget.Button[@resource-id='com.android.packageinstaller:id/permission_allow_button']");

	I.wait(1);

	I.tap("//android.widget.Button[@resource-id='com.android.packageinstaller:id/permission_allow_button']");

	I.wait(1);

	I.tap("//android.widget.ImageView[@resource-id='praktikum.develops.a11414001.digitourapplication:id/imageViewPlay']");

	I.wait(2);

	I.tap("//android.widget.Button[@text='Sign in']");

	I.wait(2);

	I.tap("//android.widget.TextView[@text='Johannes Christian Sitorus']");

	I.wait(5);

	I.tap("//android.widget.ImageButton[@content-desc='Navigate up']");

	I.wait(2);

	I.tap("//android.widget.ImageView[@resource-id='praktikum.develops.a11414001.digitourapplication:id/imageViewPlay']");

	I.wait(2);

	I.tap("//android.widget.Button[@resource-id='praktikum.develops.a11414001.digitourapplication:id/buttonLokasi']");

	I.wait(2);

	let locator = "//android.widget.LinearLayout[@bounds='[0,222][1080,2016]']";

	I.swipeUp(locator);

	I.tap("//android.widget.TextView[@text=' Siallagan, Samosir']");

	I.wait(2);

	I.see(' Batu kursi');
	
});