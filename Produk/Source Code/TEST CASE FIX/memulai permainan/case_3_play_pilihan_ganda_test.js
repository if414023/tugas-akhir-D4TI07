Feature('Case Play Game Pilihan Ganda With Click Function by get XPath that take other parameter except resource-id');

Scenario('Test Play', (I) => {
	I.wait(3);

	I.click("//android.widget.Button[@text='ALLOW']");

	I.wait(1);

	I.click("//android.widget.Button[@text='ALLOW']");

	I.wait(1);

	I.click("//android.widget.Button[@text='ALLOW']");

	I.wait(1);

	I.click("//android.widget.Button[@text='ALLOW']");

	I.wait(1);

	I.click("//android.widget.ImageView[@resource-id='praktikum.develops.a11414001.digitourapplication:id/imageViewPlay']");

	I.wait(2);

	I.click("//android.widget.Button[@text='Sign in']");

	I.wait(2);

	I.click("//android.widget.TextView[@text='Johannes Christian Sitorus']");

	I.wait(5);

	I.click("//android.widget.ImageButton[@content-desc='Navigate up']");

	I.wait(1);

	I.click("//android.widget.ImageView[@resource-id='praktikum.develops.a11414001.digitourapplication:id/imageViewPlay']");

	I.wait(2);

	I.click("//android.widget.Button[@text='GO']");

	I.wait(5);

	I.touchPerform([{
    action: 'press',
    options: {
      x: 930,
      y: 1639
    }
}, {action: 'release'}])

	I.wait(2);

	I.click("//android.view.View[@content-desc='ITDel. ']");

	I.wait(5);

	I.see('Challenge Type Di ITDel');

	I.click("//android.widget.Button[@text='PILIHAN GANDA']");

	I.wait(1);

	I.click("//android.widget.TextView[@text='1']");

	I.wait(1);

	I.click("//android.widget.Button[@text='MARTUHAN']");

	I.wait(1);

	I.click("//android.widget.Button[@text='NEXT']");

	I.wait(1);

	I.click("//android.widget.Button[@text='2']");

	I.wait(1);

	I.click("//android.widget.Button[@text='NEXT']");

	I.wait(1);

	I.click("//android.widget.Button[@text='TEKNIK INFORMATIKA']");

	I.wait(1);

	I.click("//android.widget.Button[@text='NEXT']");

	I.wait(1);

	I.click("//android.widget.Button[@text='OPEN THEATER']");

	I.wait(1);

	I.click("//android.widget.Button[@text='NEXT']");

	I.wait(1);

	I.click("//android.widget.Button[@text='523']");

	I.wait(1);

	I.click("//android.widget.Button[@text='FINISH']");

	I.wait(2);

	I.click("//android.widget.Button[@text='NEXT']");

	I.wait(2);

});