Feature('Case 2 Play Game With Tap Function by get XPath that take other parameter except resource-id');

Scenario('Test Play', (I) => {
	I.wait(3);

	I.tap("//android.widget.Button[@text='ALLOW']");

	I.wait(1);

	I.tap("//android.widget.Button[@text='ALLOW']");

	I.wait(1);

	I.tap("//android.widget.Button[@text='ALLOW']");

	I.wait(1);

	I.tap("//android.widget.Button[@text='ALLOW']");

	I.wait(1);

	I.tap("//android.widget.ImageView[@resource-id='praktikum.develops.a11414001.digitourapplication:id/imageViewPlay']");

	I.wait(2);

	I.tap("//android.widget.Button[@text='Sign in']");

	I.wait(2);

	I.tap("//android.widget.TextView[@text='Johannes Christian Sitorus']");

	I.wait(5);

	I.tap("//android.widget.ImageButton[@content-desc='Navigate up']");

	I.wait(1);

	I.tap("//android.widget.ImageView[@resource-id='praktikum.develops.a11414001.digitourapplication:id/imageViewPlay']");

	I.wait(2);

	I.tap("//android.widget.Button[@text='GO']");

	I.wait(5);

	I.touchPerform([{
    action: 'press',
    options: {
      x: 930,
      y: 1639
    }
}, {action: 'release'}])

	I.wait(2);

	I.tap("//android.view.View[@content-desc='ITDel. ']");

	I.wait(5);

	I.see('Challenge Type Di ITDel');

	I.tap("//android.widget.Button[@text='PILIHAN GANDA']");

	I.wait(1);

	I.tap("//android.widget.TextView[@text='1']");

	I.wait(1);

	I.tap("//android.widget.Button[@text='MARTUHAN']");

	I.wait(1);

	I.tap("//android.widget.Button[@text='NEXT']");

	I.wait(1);

	I.tap("//android.widget.Button[@text='2']");

	I.wait(1);

	I.tap("//android.widget.Button[@text='NEXT']");

	I.wait(1);

	I.tap("//android.widget.Button[@text='TEKNIK INFORMATIKA']");

	I.wait(1);

	I.tap("//android.widget.Button[@text='NEXT']");

	I.wait(1);

	I.tap("//android.widget.Button[@text='OPEN THEATER']");

	I.wait(1);

	I.tap("//android.widget.Button[@text='NEXT']");

	I.wait(1);

	I.tap("//android.widget.Button[@text='523']");

	I.wait(1);

	I.tap("//android.widget.Button[@text='FINISH']");

	I.wait(2);

	// I.tap("//android.widget.Button[@text='OK']");

	I.tap("//android.widget.Button[@text='NEXT']");

	I.wait(1);

	I.tap("//android.widget.Button[@text='PILIHAN GANDA']");

	I.wait(1);

	I.tap("//android.widget.TextView[@text='1']");

	I.wait(1);

	I.tap("//android.widget.Button[@text='50:50']");

	I.wait(1);

	I.tap("//android.widget.Button[@text='MARTOPPAK']");

	I.wait(1);

	I.tap("//android.widget.Button[@text='NEXT']");

	I.wait(1);

	I.tap("//android.widget.Button[@text='2']");

	I.wait(1);

	I.tap("//android.widget.Button[@text='NEXT']");

	I.wait(1);

	I.tap("//android.widget.Button[@text='TEKNIK INFORMATIKA']");

	I.wait(1);

	I.tap("//android.widget.Button[@text='NEXT']");

	I.wait(1);

	I.tap("//android.widget.Button[@text='OPEN THEATER']");

	I.wait(1);

	I.tap("//android.widget.Button[@text='NEXT']");

	I.wait(1);

	I.tap("//android.widget.Button[@text='523']");

	I.wait(1);

	I.tap("//android.widget.Button[@text='FINISH']");

	I.wait(2);

	// I.tap("//android.widget.Button[@text='OK']");

	I.tap("//android.widget.Button[@text='NEXT']");

	I.wait(1);

	I.tap("//android.widget.Button[@text='PILIHAN GANDA']");

	I.wait(1);

	I.tap("//android.widget.TextView[@text='1']");

	I.wait(1);

	I.tap("//android.widget.Button[@text='ASK FRIEND']");

	I.wait(2);

	I.tap("//android.widget.Button[@text='POST']");

	I.wait(5);

	I.tap("//android.widget.Button[@text='MARTUHAN']");

	I.wait(1);

	I.tap("//android.widget.Button[@text='NEXT']");

	I.wait(1);

	I.tap("//android.widget.Button[@text='2']");

	I.wait(1);

	I.tap("//android.widget.Button[@text='NEXT']");

	I.wait(1);

	I.tap("//android.widget.Button[@text='TEKNIK INFORMATIKA']");

	I.wait(1);

	I.tap("//android.widget.Button[@text='NEXT']");

	I.wait(1);

	I.tap("//android.widget.Button[@text='OPEN THEATER']");

	I.wait(1);

	I.tap("//android.widget.Button[@text='NEXT']");

	I.wait(1);

	I.tap("//android.widget.Button[@text='523']");

	I.wait(1);

	I.tap("//android.widget.Button[@text='FINISH']");

	I.wait(2);

	// I.tap("//android.widget.Button[@text='OK']");

	I.tap("//android.widget.Button[@text='NEXT']");

	I.wait(1);

	I.tap("//android.widget.Button[@text='PILIHAN GANDA']");

	I.wait(1);

	I.tap("//android.widget.TextView[@text='1']");

	I.wait(1);

	I.tap("//android.widget.Button[@text='MARTUHAN']");

	I.wait(1);

	I.tap("//android.widget.Button[@text='NEXT']");

	I.wait(1);

	I.tap("//android.widget.Button[@text='2']");

	I.wait(1);

	I.tap("//android.widget.Button[@text='PREV']");

	I.wait(1);

	I.tap("//android.widget.Button[@text='MARTOPPAK']");

	I.wait(1);

	I.tap("//android.widget.Button[@text='NEXT']");

	I.wait(1);

	I.tap("//android.widget.Button[@text='2']");

	I.wait(1);

	I.tap("//android.widget.Button[@text='NEXT']");

	I.wait(1);

	I.tap("//android.widget.Button[@text='TEKNIK INFORMATIKA']");

	I.wait(1);

	I.tap("//android.widget.Button[@text='NEXT']");

	I.wait(1);

	I.tap("//android.widget.Button[@text='OPEN THEATER']");

	I.wait(1);

	I.tap("//android.widget.Button[@text='NEXT']");

	I.wait(1);

	I.tap("//android.widget.Button[@text='523']");

	I.wait(1);

	I.tap("//android.widget.Button[@text='FINISH']");

	I.wait(2);

	I.tap("//android.widget.Button[@text='NEXT']");

	I.wait(1);

	I.tap("//android.widget.Button[@text='TRUE OR FALSE']");

	I.wait(1);

	I.see('True False');

	I.wait(1);

	I.tap("//android.widget.TextView[@text='1']");

	I.wait(1);

	I.tap("//android.widget.Button[@text='TRUE']");

	I.wait(1);

	I.tap("//android.widget.Button[@text='NEXT']");

	I.wait(1);

	I.tap("//android.widget.Button[@text='TRUE']");

	I.wait(1);

	I.tap("//android.widget.Button[@text='NEXT']");

	I.wait(1);

	I.tap("//android.widget.Button[@text='TRUE']");

	I.wait(1);

	I.tap("//android.widget.Button[@text='NEXT']");

	I.wait(1);

	I.tap("//android.widget.Button[@text='TRUE']");

	I.wait(1);

	I.tap("//android.widget.Button[@text='NEXT']");

	I.wait(1);

	I.tap("//android.widget.Button[@text='TRUE']");

	I.wait(1);

	I.tap("//android.widget.Button[@text='FINISH']");

	I.wait(2);

	I.tap("//android.widget.Button[@text='NEXT']");

	I.wait(1);

	I.tap("//android.widget.Button[@text='TRUE OR FALSE']");

	I.wait(1);

	I.tap("//android.widget.TextView[@text='1']");

	I.wait(1);

	I.tap("//android.widget.Button[@text='TRUE']");

	I.wait(1);

	I.tap("//android.widget.Button[@text='NEXT']");

	I.wait(1);

	I.tap("//android.widget.Button[@text='PREV']");

	I.wait(1);

	I.see('Disediakannya tempat futsal.');

	I.wait(1);

	I.tap("//android.widget.Button[@text='FALSE']");

	I.wait(1);

	I.tap("//android.widget.Button[@text='NEXT']");

	I.wait(1);

	I.tap("//android.widget.Button[@text='TRUE']");

	I.wait(1);

	I.tap("//android.widget.Button[@text='NEXT']");

	I.wait(1);

	I.tap("//android.widget.Button[@text='TRUE']");

	I.wait(1);

	I.tap("//android.widget.Button[@text='NEXT']");

	I.wait(1);

	I.tap("//android.widget.Button[@text='TRUE']");

	I.wait(1);

	I.tap("//android.widget.Button[@text='NEXT']");

	I.wait(1);

	I.tap("//android.widget.Button[@text='TRUE']");

	I.wait(1);

	I.tap("//android.widget.Button[@text='FINISH']");

	I.wait(1);

	I.tap("//android.widget.Button[@text='NEXT']");

	I.wait(1);

	I.tap("//android.widget.Button[@text='AUGMENTED REALITY']");

	I.wait(1);

	I.tap("//android.widget.Button[@text='VIEW IMAGE']");

	I.wait(1);

	I.tap("//android.widget.Button[@text='BACK']");

	I.wait(1);

	I.tap("//android.widget.Button[@text='INFO']");

	I.wait(1);

	I.tap("//android.widget.Button[@text='OK']");

	I.wait(1);

	I.tap("//android.widget.Button[@text='AUDIO DESC']");	

	I.wait(28);
});