Feature('Case 2 Play Game With Tap Function by get XPath');

Scenario('Test Play', (I) => {
	I.wait(3);

	I.tap("//android.widget.Button[@resource-id='com.android.packageinstaller:id/permission_allow_button']");

	I.wait(1);

	I.tap("//android.widget.Button[@resource-id='com.android.packageinstaller:id/permission_allow_button']");

	I.wait(1);

	I.tap("//android.widget.Button[@resource-id='com.android.packageinstaller:id/permission_allow_button']");

	I.wait(1);

	I.tap("//android.widget.Button[@resource-id='com.android.packageinstaller:id/permission_allow_button']");

	I.wait(1);

	I.tap("//android.widget.ImageView[@resource-id='praktikum.develops.a11414001.digitourapplication:id/imageViewPlay']");

	I.wait(2);

	I.tap("//android.widget.Button[@text='Sign in']");

	I.wait(2);

	I.tap("//android.widget.TextView[@text='Johannes Christian Sitorus']");

	I.wait(5);

	I.tap("//android.widget.ImageButton[@content-desc='Navigate up']");

	I.wait(1);

	I.tap("//android.widget.ImageView[@resource-id='praktikum.develops.a11414001.digitourapplication:id/imageViewPlay']");

	I.wait(2);

	I.tap("//android.widget.Button[@resource-id='praktikum.develops.a11414001.digitourapplication:id/buttonGo']");

	I.wait(5);

	I.touchPerform([{
    action: 'press',
    options: {
      x: 930,
      y: 1639
    }
}, {action: 'release'}])

	I.wait(2);

	I.tap("//android.view.View[@content-desc='ITDel. ']");

	I.wait(5);

	I.see('Challenge Type Di ITDel');

	I.see('PILIHAN GANDA');

	I.see('TRUE OR FALSE');

	I.see('AUGMENTED REALITY');
});