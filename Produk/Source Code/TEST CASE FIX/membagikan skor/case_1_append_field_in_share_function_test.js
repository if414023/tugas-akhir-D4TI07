Feature('Case 10 Append Field with Click Function in Share Function');

Scenario('Test Share', (I) => {
	I.wait(3);

	I.click("//android.widget.Button[@resource-id='com.android.packageinstaller:id/permission_allow_button']");

	I.wait(1);

	I.click("//android.widget.Button[@resource-id='com.android.packageinstaller:id/permission_allow_button']");

	I.wait(1);

	I.click("//android.widget.Button[@resource-id='com.android.packageinstaller:id/permission_allow_button']");

	I.wait(1);

	I.click("//android.widget.Button[@resource-id='com.android.packageinstaller:id/permission_allow_button']");

	I.wait(1);
	
	I.click("//android.widget.ImageView[@resource-id='praktikum.develops.a11414001.digitourapplication:id/btnImageShareFacebook']");

	I.wait(2);

	I.appendField('Write something…', 'Saya mau share ke facebook dengan cara append field');

	I.wait(1);

	I.click("//android.widget.Button[@text='POST']");

	I.wait(3);
});