Feature('Case 4 Leaderboard With Tap Function by get XPath');

Scenario('Test Leaderboard', (I) => {
	I.wait(3);

	I.tap("//android.widget.Button[@resource-id='com.android.packageinstaller:id/permission_allow_button']");

	I.wait(1);

	I.tap("//android.widget.Button[@resource-id='com.android.packageinstaller:id/permission_allow_button']");

	I.wait(1);

	I.tap("//android.widget.Button[@resource-id='com.android.packageinstaller:id/permission_allow_button']");

	I.wait(1);

	I.tap("//android.widget.Button[@resource-id='com.android.packageinstaller:id/permission_allow_button']");

	I.wait(1);

	I.tap("//android.widget.ImageView[@resource-id='praktikum.develops.a11414001.digitourapplication:id/imageViewLeaderboard']");

	I.wait(1);

	I.see('Name');

	I.wait(1);

	I.see('Location');

	I.wait(1);

	I.see('Type');

	I.wait(1);

	I.see('Score');

	I.wait(1);

	I.see('Time');

	I.wait(3);
});