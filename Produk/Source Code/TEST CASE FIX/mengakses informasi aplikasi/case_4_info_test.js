Feature('Case 6 Info With Tap Function by get XPath that take other parameter except resource-id');

Scenario('Test Info', (I) => {
	I.wait(3);

	I.tap("//android.widget.Button[@text='ALLOW']");

	I.wait(1);

	I.tap("//android.widget.Button[@text='ALLOW']");

	I.wait(1);

	I.tap("//android.widget.Button[@text='ALLOW']");

	I.wait(1);

	I.tap("//android.widget.Button[@text='ALLOW']");

	I.wait(1);
	
	I.tap("//android.widget.ImageView[@bounds='[0,1422][540,2016]']");

	I.wait(1);

	I.see('Informasi Digitour');

	I.wait(3);
});